﻿using MvvmCross.IoC;
using MvvmCross.ViewModels;

namespace iTranslator.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes().EndingWith("Repository").AsInterfaces().RegisterAsLazySingleton();
            CreatableTypes().EndingWith("Service").AsInterfaces().RegisterAsLazySingleton();
            RegisterCustomAppStart<AppStart>();
        }
    }
}