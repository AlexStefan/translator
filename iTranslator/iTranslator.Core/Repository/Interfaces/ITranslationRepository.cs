﻿using System.Collections.Generic;
using System.Threading.Tasks;
using iTranslator.Models;

namespace iTranslator.Core.Repository.Interfaces
{
    public interface ITranslationRepository
    {
        Task<List<Word>> LoadWords();
    }
}