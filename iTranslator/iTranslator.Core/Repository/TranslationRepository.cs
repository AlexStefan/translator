﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;
using iTranslator.Core.Repository.Interfaces;
using iTranslator.Models;

namespace iTranslator.Core.Repository
{
    public class TranslationRepository : ITranslationRepository
    {
        public async Task<List<Word>> LoadWords()
        {
            var words = new List<Word>();
            var doc = XDocument.Load(typeof(TranslationRepository).Assembly.GetManifestResourceStream("iTranslator.Core.Resources.Intelligent_translator.xml"));
            foreach (var node in doc.Descendants("RECORD"))
            {
                var word = new Word();
                word.Value = node.Attribute("word").Value;
                word.Language = node.Attribute("culture").Value;
                foreach (var subnode in node.Descendants("LINK"))
                {
                    var relatedWord = new Word();
                    relatedWord.Value = subnode.Attribute("word").Value;
                    relatedWord.Language = subnode.Attribute("culture").Value;
                    word.RelatedWords.Add(relatedWord);
                }
                words.Add(word);
            }
            return words;
        }
    }
}