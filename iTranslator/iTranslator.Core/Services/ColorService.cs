﻿using iTranslator.Core.Resources;
using iTranslator.Core.Services.Interfaces;
using MvvmCross.UI;

namespace iTranslator.Core.Services
{
    public class ColorService : IColorService
    {
        public MvxColor Convert(ColorEnum value)
        {
            var color = new MvxColor(0, 0, 0);
            switch (value)
            {
                case ColorEnum.Green:
                    color = new MvxColor(68, 150, 109);
                    break;
                case ColorEnum.Red:
                    color = new MvxColor(201, 61, 52);
                    break;
                case ColorEnum.White:
                    color = new MvxColor(255, 255, 255);
                    break;
                default:
                    break;
            }
            return color;
        }
    }
}