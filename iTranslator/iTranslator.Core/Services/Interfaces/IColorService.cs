﻿using iTranslator.Core.Resources;
using MvvmCross.UI;

namespace iTranslator.Core.Services.Interfaces
{
    public interface IColorService
    {
        MvxColor Convert(ColorEnum value);
    }
}