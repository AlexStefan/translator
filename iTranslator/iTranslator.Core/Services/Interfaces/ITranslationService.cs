﻿using System.Threading.Tasks;
using iTranslator.Models;

namespace iTranslator.Core.Services.Interfaces
{
    public interface ITranslationService
    {
        Task<Word> FindWord(string value);
    }
}