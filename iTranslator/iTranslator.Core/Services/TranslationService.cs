﻿using System.Linq;
using System.Threading.Tasks;
using iTranslator.Core.Repository.Interfaces;
using iTranslator.Core.Services.Interfaces;
using iTranslator.Models;

namespace iTranslator.Core.Services
{
    public class TranslationService : ITranslationService
    {
        private readonly ITranslationRepository translationRepository;

        public TranslationService(ITranslationRepository translationRepository)
        {
            this.translationRepository = translationRepository;
        }

        public async Task<Word> FindWord(string value)
        {
            var wordsCollection = await translationRepository.LoadWords();
            var matchWord = wordsCollection.FirstOrDefault(w => w.Value.ToLower().Equals(value.ToLower()));
            //XML processing was too fast and loading was never visible -> duplicated the file until 15k lines but as we're interested on how the UI will 
            //  react if there is something in background I thought that this is more effective.
            await Task.Delay(1000);
            return matchWord;
        }
    }
}