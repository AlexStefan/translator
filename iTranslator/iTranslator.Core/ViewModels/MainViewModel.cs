﻿using System.Threading.Tasks;
using iTranslator.Core.Resources;
using iTranslator.Core.Services.Interfaces;
using iTranslator.Models;
using MvvmCross.UI;
using MvvmCross.ViewModels;

namespace iTranslator.Core.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        private readonly IColorService colorService;
        private readonly ITranslationService translationService;

        private string word;
        public string Word
        {
            get
            {
                return word;
            }
            set
            {
                word = value;
#pragma warning disable CS4014 // Feature, not bug. Because this call is not awaited, execution of the current method continues before the call is completed
                ProcessInput(value);
#pragma warning restore CS4014 // Feature, not bug. Because this call is not awaited, execution of the current method continues before the call is completed
                RaisePropertyChanged(() => Word);
            }
        }

        private Word result;
        public Word Result
        {
            get
            {
                return result;
            }
            set
            {
                result = value;
                RaisePropertyChanged(() => IsStaticInfoInvisible);
                RaisePropertyChanged(() => Result);
            }
        }

        private bool isSpinnerVisible;
        public bool IsSpinnerVisible
        {
            get
            {
                return isSpinnerVisible;
            }
            set
            {
                isSpinnerVisible = value;
                RaisePropertyChanged(() => IsSpinnerVisible);
            }
        }

        public bool IsStaticInfoInvisible
        {
            get
            {
                return Result == null || string.IsNullOrEmpty(Result.Language);
            }
        }

        private MvxColor backgroundColor;
        public MvxColor BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                RaisePropertyChanged(() => BackgroundColor);
            }
        }

        private async Task ProcessInput(string userInput)
        {
            IsSpinnerVisible = true;
            if (userInput.Length > 2)
            {
                Result = await translationService.FindWord(userInput);
                if (Result != null)
                    BackgroundColor = colorService.Convert(ColorEnum.Green);
                else if (Word.Length == 0)
                    BackgroundColor = colorService.Convert(ColorEnum.White);
                else
                    BackgroundColor = colorService.Convert(ColorEnum.Red);
            }
            else
                BackgroundColor = colorService.Convert(ColorEnum.White);
            IsSpinnerVisible = false;
        }

        public MainViewModel(IColorService colorService, ITranslationService translationService)
        {
            this.colorService = colorService;
            this.translationService = translationService;
            IsSpinnerVisible = false;
            Result = new Word();
        }
    }
}