﻿using System;
using Android.App;
using Android.Runtime;
using iTranslator.Core;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace iTranslator.Droid
{
    [Application]
    public class MainApplication : MvxAppCompatApplication<Setup, App>
    {
        public MainApplication()
        {
        }

        public MainApplication(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }
    }
}