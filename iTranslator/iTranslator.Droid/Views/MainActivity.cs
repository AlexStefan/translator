﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Support.V7.AppCompat;
using iTranslator.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using Android.Widget;

namespace iTranslator.Droid.Views
{
    [Activity(Label = "iTranslator.Droid", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : MvxAppCompatActivity<MainViewModel>
    {
        private ProgressBar progressBar;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            Title = Resources.GetString(Resource.String.main_title);

            progressBar = FindViewById<ProgressBar>(Resource.Id.progress);

            var set = this.CreateBindingSet<MainActivity, MainViewModel>();
            set.Bind(this).For(x => x.IsActivityIndicatorActive).To(vm => vm.IsSpinnerVisible);
            set.Apply();
        }


        private bool isActivityIndicatorActive;
        public bool IsActivityIndicatorActive
        {
            get
            {
                return isActivityIndicatorActive;
            }
            set
            {
                isActivityIndicatorActive = value;
                if (value)
                {
                    progressBar.Visibility = Android.Views.ViewStates.Visible;

                }
                else
                {
                    progressBar.Visibility = Android.Views.ViewStates.Invisible;
                }
            }
        }
    }
}