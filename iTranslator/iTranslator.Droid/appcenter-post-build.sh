﻿#!/usr/bin/env bash
#
# For Xamarin, run all XUnit test projects that have "Test" in the name.
# The script will build, run and display the results in the build logs.

echo "Found XUnit test projects:"
find $APPCENTER_SOURCE_DIRECTORY -regex '.*Tests.csproj' -exec echo {} \;
echo
echo "Building XUnit test projects:"
find $APPCENTER_SOURCE_DIRECTORY -regex '.*Tests.csproj' -exec msbuild {} \;
echo
echo "Compiled projects to run XUnit tests:"
find $APPCENTER_SOURCE_DIRECTORY -regex '.*bin.*Tests.dll' -exec echo {} \;
echo
echo "Running XUnit tests:"
cd $APPCENTER_SOURCE_DIRECTORY/iTranslator/iTranslator.Tests 
dotnet test
echo
echo "XUnit tests result:"
pathOfTestResults=$(find $APPCENTER_SOURCE_DIRECTORY -name 'TestResult.xml')
cat $pathOfTestResults
echo

#look for a failing test
grep -q 'success="False"' $pathOfTestResults

if [[ $? -eq 0 ]]
then 
echo "A test Failed" 
exit 1
else 
echo "All tests passed" 
fi