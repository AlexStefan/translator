﻿using System.Collections.Generic;

namespace iTranslator.Models
{
    public class Word
    {
        public string Value { get; set; }
        public string Language { get; set; }
        public List<Word> RelatedWords { get; set; }

        public Word()
        {
            RelatedWords = new List<Word>();
        }

        public Word(string value, string language)
        {
            Value = value;
            Language = language;
            RelatedWords = new List<Word>();
        }
    }
}