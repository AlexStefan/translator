﻿using iTranslator.Core.Resources;
using iTranslator.Core.Services.Interfaces;
using iTranslator.Core.ViewModels;
using iTranslator.Models;
using Moq;
using MvvmCross.UI;
using Xunit;

namespace iTranslator.Tests.ViewModels
{
    public class MainViewModelTests
    {
        [Fact]
        public void TestOneLetterInputWord()
        {
            //Arrange
            var mockColorService = new Mock<IColorService>();
            mockColorService.Setup(t => t.Convert(It.IsAny<ColorEnum>())).Returns(new MvxColor(255, 255, 255));
            var mockTranslationService = new Mock<ITranslationService>();
            mockTranslationService.Setup(t => t.FindWord(It.IsAny<string>())).ReturnsAsync(new Word());

            var mainViewModel = new MainViewModel(mockColorService.Object, mockTranslationService.Object);

            //Act
            mainViewModel.Word = "t";

            //Assert
            Assert.False(mainViewModel.IsSpinnerVisible);
            Assert.Null(mainViewModel.Result.Value);
            Assert.True(mainViewModel.IsStaticInfoInvisible);
        }

        [Fact]
        public void TestRightInputWord()
        {
            //Arrange
            var mockColor = new MvxColor(68, 150, 109);
            var mockColorService = new Mock<IColorService>();
            mockColorService.Setup(t => t.Convert(It.IsAny<ColorEnum>())).Returns(mockColor);
            var mockTranslationService = new Mock<ITranslationService>();
            mockTranslationService.Setup(t => t.FindWord(It.IsAny<string>())).ReturnsAsync(new Word("result", "en-US"));

            var mainViewModel = new MainViewModel(mockColorService.Object, mockTranslationService.Object);

            //Act
            mainViewModel.Word = "result";

            //Assert
            Assert.Equal("en-US", mainViewModel.Result.Language);
            Assert.Equal(mockColor, mainViewModel.BackgroundColor);
            Assert.False(mainViewModel.IsStaticInfoInvisible);
        }

        [Fact]
        public void TestWrongInputWord()
        {
            //Arrange
            var mockColor = new MvxColor(201, 61, 52);
            var mockColorService = new Mock<IColorService>();
            mockColorService.Setup(t => t.Convert(It.IsAny<ColorEnum>())).Returns(mockColor);
            var mockTranslationService = new Mock<ITranslationService>();
            mockTranslationService.Setup(t => t.FindWord(It.IsAny<string>())).ReturnsAsync(() => null);

            var mainViewModel = new MainViewModel(mockColorService.Object, mockTranslationService.Object);

            //Act
            mainViewModel.Word = "res5%";

            //Assert
            Assert.Null(mainViewModel.Result);
            Assert.Equal(mockColor, mainViewModel.BackgroundColor);
            Assert.True(mainViewModel.IsStaticInfoInvisible);
        }

        [Fact]
        public void TestEmptyInputWord()
        {
            //Arrange
            var mockColor = new MvxColor(255, 255, 255);
            var mockColorService = new Mock<IColorService>();
            mockColorService.Setup(t => t.Convert(It.IsAny<ColorEnum>())).Returns(mockColor);
            var mockTranslationService = new Mock<ITranslationService>();

            var mainViewModel = new MainViewModel(mockColorService.Object, mockTranslationService.Object);

            //Act
            mainViewModel.Word = string.Empty;

            //Assert
            Assert.Null(mainViewModel.Result.Value);
            Assert.Equal(mockColor, mainViewModel.BackgroundColor);
            Assert.True(mainViewModel.IsStaticInfoInvisible);
        }
    }
}