﻿using CoreGraphics;
using Foundation;
using iTranslator.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Views;
using UIKit;

namespace iTranslator.Touch.Views
{
    public partial class MainViewController : MvxViewController<MainViewModel>
    {
        UIActivityIndicatorView activityIndicatorView;

        public MainViewController() : base("MainViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            AddActivityIndicator();
            Title = NSBundle.MainBundle.GetLocalizedString("title", "");
            lbLinkedWords.Text = NSBundle.MainBundle.GetLocalizedString("linked_words", "");
            lbLanguageInfo.Text = NSBundle.MainBundle.GetLocalizedString("language", "");
            txtWord.Placeholder = NSBundle.MainBundle.GetLocalizedString("type_the_word", "");

            var source = new MvxSimpleTableViewSource(tbWords, "WordViewCell", WordViewCell.Key);

            var set = this.CreateBindingSet<MainViewController, MainViewModel>();
            set.Bind(txtWord).To(vm => vm.Word);
            set.Bind(lbResult).To(vm => vm.Result.Language);
            set.Bind(source).To(vm => vm.Result.RelatedWords);
            set.Bind(this).For(x => x.IsActivityIndicatorActive).To(vm => vm.IsSpinnerVisible); 
            set.Bind(txtWord).For(x => x.BackgroundColor).To(vm => vm.BackgroundColor).WithConversion("NativeColor");
            set.Bind(lbLanguageInfo).For(x => x.Hidden).To(vm => vm.IsStaticInfoInvisible);
            set.Bind(lbLinkedWords).For(x => x.Hidden).To(vm => vm.IsStaticInfoInvisible);
            set.Apply();

            tbWords.Source = source;
            tbWords.TableFooterView = new UIView();
            tbWords.ReloadData();
        }

        private void AddActivityIndicator()
        {
            activityIndicatorView = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
            //TODO fix right space issue on iPhone 8 Plus and similar
            activityIndicatorView.Frame = new CGRect(txtWord.Bounds.Width - activityIndicatorView.Frame.Width, 0, activityIndicatorView.Frame.Width, txtWord.Bounds.Height);
            txtWord.AddSubview(activityIndicatorView);
        }

        private bool isActivityIndicatorActive;
        public bool IsActivityIndicatorActive
        {
            get
            {
                return isActivityIndicatorActive;
            }
            set
            {
                isActivityIndicatorActive = value;
                if (value)
                {
                    activityIndicatorView.StartAnimating();

                }
                else
                {
                    activityIndicatorView.StopAnimating();
                }
            }
        }
    }
}