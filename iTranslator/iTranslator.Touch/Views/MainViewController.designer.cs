// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace iTranslator.Touch.Views
{
    [Register ("MainViewController")]
    partial class MainViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbLanguageInfo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbLinkedWords { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbResult { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tbWords { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txtWord { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lbLanguageInfo != null) {
                lbLanguageInfo.Dispose ();
                lbLanguageInfo = null;
            }

            if (lbLinkedWords != null) {
                lbLinkedWords.Dispose ();
                lbLinkedWords = null;
            }

            if (lbResult != null) {
                lbResult.Dispose ();
                lbResult = null;
            }

            if (tbWords != null) {
                tbWords.Dispose ();
                tbWords = null;
            }

            if (txtWord != null) {
                txtWord.Dispose ();
                txtWord = null;
            }
        }
    }
}