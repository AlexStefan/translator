﻿using System;
using Foundation;
using iTranslator.Models;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;

namespace iTranslator.Touch.Views
{
    public partial class WordViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("WordViewCell");
        public static readonly UINib Nib;

        static WordViewCell()
        {
            Nib = UINib.FromName("WordViewCell", NSBundle.MainBundle);
        }

        protected WordViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<WordViewCell, Word>();
                set.Bind(lbRelatedWord).To(w => w.Value);
                set.Bind(lbRelatedLanguage).To(w => w.Language);
                set.Apply();
            });
        }
    }
}