﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace iTranslator.Touch.Views
{
    [Register ("WordViewCell")]
    partial class WordViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbRelatedLanguage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbRelatedWord { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lbRelatedLanguage != null) {
                lbRelatedLanguage.Dispose ();
                lbRelatedLanguage = null;
            }

            if (lbRelatedWord != null) {
                lbRelatedWord.Dispose ();
                lbRelatedWord = null;
            }
        }
    }
}